using LinearAlgebra

# minimise convex function subject to convex ≤ constraints
function ellipse_minimise(c, P, obj, cons; verbose=false, tol = 1e-5, maxiter=1000)
    dim = length(c);

    c = copy(c);   # wreck a copy
    P = Matrix{Float64}(P); # wreck a copy.
    Pg = copy(c);  # preallocate
    gb = copy(c); # gradient buffer

    # store best iterate
    bestpt = copy(c); # answer buffer
    bestinfo = nothing


    for it in 1:maxiter
        # check any constraint violation first
        viol, cg = cons(c)

        g = if viol > 0 # strict violation
            @assert norm(cg) > 0;
            cg
        else
            info = obj(c) # (value, gradient, extra...)
            val, grad = info;
            if bestinfo === nothing || val < bestinfo[1]
                bestpt .= c;
                bestinfo = info
            end
            grad
        end

        # we got our gradient, do a step!
        gb .= g; # write Tuple to pre-allocated Array
        mul!(Pg, P, gb); # Pg .= P*gb;
        gPg = g ⋅ Pg;

        if verbose
            if viol ≤ 0
                println("Objective round ‖g‖_P is $(sqrt(max(0,gPg))), current volume is $(sqrt(max(0,det(P)))), best so far $(bestinfo[1])");
            else
                println("Constraint round ‖g‖_P is $(sqrt(max(0,gPg))), current volume is $(sqrt(max(0,det(P)))), current viol is $viol")
            end
        end

        if viol ≤ 0 && gPg < tol^2
            # √gPg ≥ sub-optimality of bestpt. We are done!
            @assert bestinfo !== nothing
            return bestpt, bestinfo
        end

        if Inf > viol > 0 && gPg < viol^2
            @warn "Remaining ellipse is infeasible since $(sqrt(max(0,gPg))) < $viol. Mathematically this can only happen if your problem is infeasible. Anway, bailing now. Bye!"
            return bestpt, bestinfo
        end

        if gPg ≤ 0
            @warn "$g has zero norm in $P at $c. Infeasible problem? Bailing now. Bye!"
            return bestpt, bestinfo
        end

        #@assert gPg > 0 "$g has zero norm in $P at $c. Infeasible problem?";

        Pg ./= sqrt(gPg) # normalise

        # update center
        c .-= 1/(dim+1) .* Pg

        # update radii
        if dim == 1
            P ./= sqrt(2); # binary search
        else
            P .= (dim^2/(dim^2-1)) .* (P .- (2/(dim+1)) .* (Pg.*Pg'));
        end

        @assert issymmetric(P) "$P not PSD" # in fact PSD, but that's expensive
    end

    @assert false "Ellipse ran out of iteration budget. Current volume is $(sqrt(max(0,det(P))))"
    return bestpt, bestinfo

end



function neg_val_grad(v, g, info...)
    -v, .-g, info...
end


function neg_val(v, g, info...)
    -v, g, info...
end



# maximise concave function subject to convex ≤ constraints
function ellipse_maximise(c, P, obj, cons; verbose=false, tol = 1e-5, maxiter=1000)
    pt, vginfo = ellipse_minimise(
        c, P,
        c -> neg_val_grad(obj(c)...),
        cons; verbose=verbose, tol=tol, maxiter=maxiter)
    pt, neg_val(vginfo...)
end






# wrapper to deal with linear equality constraints
function equality_wrap(f, cons, c, P, A, b)

    q = svd(A, full=true);
    m = length(b);

    Q = q.V[:,m+1:end]
    z = q.V[:,1:m]*(q.U'b ./ q.S);

    w(x) = Q*x .+ z
    x(w) = Q'w

    @assert isapprox(x(w(x(c))), x(c), atol=1e-5)

    warp∇((v,∇)) = v, vec(Q'∇)
    fA(x)    = warp∇(f(w(x)))
    consA(x) = warp∇(cons(w(x)))

    cA = x(c)
    PA = Matrix(Symmetric(Q'*P*Q))

    fA, consA, cA, PA, w, x
end


# wrapper to deal with unit sum constraints
function unitsum_wrap(f, cons, c, P)
    equality_wrap(f, cons, c, P, ones(1, length(c)), 1.)
end
