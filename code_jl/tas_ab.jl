# Identify *all arms* among μ[2:end] that are better than the baseline μ[1]
# using Track-and-Stop


using Test, Random, Plots
include("general.jl")
include("libs/tidnabbil/regret.jl")
include("libs/py_pickle.jl")

# correct answer function
function istar(μ, β)
    quals = μ*β
    if false # BAI
        argmax(quals)
    else # A/B
        quals[2:end] .≥ quals[1]
    end
end


function pval(min_glr, t, delta)
    (log(t) + 1) / exp(min_glr)
end


# compute the GLR with one designated 'base' arm, by
# taking the minimum over all other arms
function GLR_min_arm(w, μ, expfam, base, β, GLR_strucfun)
    minimum((GLR_strucfun(w, μ, expfam, base, oth, β)[1], oth)
            for oth in 1:size(μ,1) if oth ≠ base
            )
end


function uniform(rng, K, J, expfam, df, GLR, β, α, μ, T, δ)
  # Chernoff stopping rule, uniform sampling rule
    condition = true

    # initialization
    N = zeros(Int64, K, J) .+ 1
    S = zeros(K, J) .+ 1/2

    p = 1
    t = 0
    res = []

    while (condition)
        # i.i.d. contexts
        Jt = findfirst(rand(rng) .≤ cumsum(α));
        @assert Jt != nothing

        # take the sample in the Bandit
        season = Jt - 1
        for action in 1:K
            S[action, Jt] += df[action-1][season][N[action, Jt]]
            N[action, Jt] += 1
        end

        μhat = S./N
        v, _ = GLR(expfam, N, μhat) # evaluate GLR statistic for stopping
        p = minimum((p, pval(v, sum(N), δ)))
        t = sum(N)

        push!(res, (t, v, p, istar(μhat, β) == istar(μ, β)))

        if t >= T
            t_δ = T;
            condition=false
        end

        if v >= log((log(sum(N))+1)/δ)
            t_δ = t;
            condition = false
        end
    end
    res, N, S
end


function BC_ABC(rng, K, J, expfam, df, GLR, β, α, μ, T, δ)
    condition = true

    # initialization
    N = zeros(Int64, K, J) .+ 1
    S = zeros(K, J) .+ 1/2

    p = 1
    t = 0
    res = []

    while (condition)
        # i.i.d. contexts
        Jt = findfirst(rand(rng) .≤ cumsum(α));
        @assert Jt != nothing

        Iprop = findfirst(sum(N, dims=2) .≤ sqrt(sum(N)))

        IJ = if Iprop === nothing
            μhat = S./N
            # evaluate GLR statistic for stopping and closest arm for sampling
            v, i = GLR_min_arm(N, μhat, expfam, 1, β, GLR_newton_unstructured)
            p = minimum((p, pval(v, sum(N), δ)))
            push!(res, (t, v, p, istar(μhat, β) == istar(μ, β)))

            if v >= log((log(sum(N))+1)/δ)
                t_δ = t;
                break
            end

            # check stopping condition
            if t >= T
                t_δ = T;
                condition = false
            end
            (i, 1)
        else
            Iprop[1]
        end


        for ij in 1:length(IJ) # could be just 2
            action = IJ[ij] - 1
            season = Jt - 1
            S[IJ[ij], Jt] += df[action][season][N[IJ[ij], Jt]]
            N[IJ[ij], Jt] += 1
        end

        t += length(IJ)

    end

    res, N, S
   end


function tas_oblivious(rng, K, J, expfam, df, GLR, β, α, μ, T, δ)

    # mild fake outcomes cheat to regularize Bernoulli
    N = zeros(Int64, K, 1) .+ 1
    S = zeros(K, 1) .+ 1/2
    N_data = zeros(Int64, K, J) .+ 1  # for reading the data

    GLR = (expfam, w, μ) -> GLR_allbetter(w, μ, expfam, ones(1,1), GLR_newton_unstructured)

    # version of exponentiated gradient,
    # used as our online learner for w
    ah = AdaHedge(K)

    res = []
    p = 1

    for t in 1:T
        # i.i.d. contexts
        Jt = findfirst(rand(rng) .≤ cumsum(α));
        @assert Jt != nothing

        # forced exploration
        Iprop = findfirst(N .≤ sqrt(sum(N)))

        It = if Iprop === nothing
            μhat = S./N
            v, _ = GLR(expfam, N, μhat) # evaluate GLR statistic for stopping
            p = minimum((p, pval(v, sum(N), δ)))

            push!(res, (t, v, p, istar(μhat, ones(1,1)) == istar(μ,β)))

            if v >= log((log(sum(N))+1)/δ)
                t_δ = t;
                break
            end

            # get the online learner's current weights
            w = reshape(act(ah), K, 1)

            # direct tracking
            argmin(N .- (sum(N)+1).*w)[1]
        else
            Iprop[1]
        end

        # take the sample in the Bandit
        action = It - 1
        season = Jt - 1
        S[It, 1] += df[action][season][N_data[It, Jt]]
        N[It, 1] += 1
        N_data[It, Jt] += 1

        # and perform one gradient step
        if Iprop === nothing
            _, ∇ = GLR(expfam, w, S./N) # evaluate GLR statistic for gradient
            incur!(ah, -∇[:])
        end
    end

    res, N, S
end


function tas_agnostic(rng, K, J, expfam, df, GLR, β, α, μ, T, δ)

    # mild fake outcomes cheat to regularize Bernoulli
    N = zeros(Int64, K, J) .+ 1
    S = zeros(K, J) .+ 1/2

    # version of exponentiated gradient,
    # used as our online learner for w
    ah = AdaHedge(K)

    # vector of results
    res = []
    p = 1

    for t in 1:T
        # i.i.d. contexts
        Jt = findfirst(rand(rng) .≤ cumsum(α));
        @assert Jt != nothing

        # forced exploration
        Iprop = findfirst(sum(N, dims=2) .≤ sqrt(sum(N)))

        It = if Iprop === nothing
            μhat = S./N
            v, _ = GLR(expfam, N, μhat) # evaluate GLR statistic for stopping
            p = minimum((p, pval(v, sum(N), δ)))

            push!(res, (t, v, p, istar(μhat, β) == istar(μ, β)))

            if v >= log((log(sum(N))+1)/δ)
                t_δ = t;
                break
            end

            # get the online learner's current weights
            w = act(ah)

            # direct tracking
            argmin(sum(N, dims=2) .- (sum(N)+1).*w)[1]
        else
            Iprop[1]
        end

        # take the sample in the Bandit
        action = It - 1
        season = Jt - 1
        S[It, Jt] += df[action][season][N[It, Jt]]
        N[It, Jt] += 1

        # and perform one gradient step
        if Iprop === nothing
            _, ∇ = GLR(expfam, w*α', S./N) # evaluate GLR statistic for gradient
            incur!(ah, -∇*α)
        end
    end

    res, N, S
end


function tas_active(K, J, expfam, df, GLR, β, α, μ, T, δ; verbose=false)

    # mild fake outcomes cheat to regularize Bernoulli
    N = zeros(Int64, K, J) .+ 1
    S = zeros(K, J) .+ 1/2

    # version of exponentiated gradient,
    # used as our online learner for w
    ah = AdaHedge(K*J)

    # vector of results
    res = []
    p = 1

    for t in 1:T
        # forced exploration
        IJprop = findfirst(N .≤ sqrt(sum(N)))

        IJ = if IJprop === nothing
            μhat = S./N
            v, _ = GLR(expfam, N, μhat) # evaluate GLR statistic for stopping
            p = minimum((p, pval(v, sum(N),δ)))

            push!(res, (t, v, p, istar(μhat, β) == istar(μ,β))) # if we would stop now ...

            # get the online learner's current weights
            w = reshape(act(ah), K, J)

            # direct tracking
            argmin(N .- (sum(N)+1).*w)
        else
            IJprop
        end

        # take the sample in the Bandit
        action = IJ[1] - 1
        season = IJ[2] - 1
        S[IJ] += df[action][season][N[IJ]]
        N[IJ] += 1

        # and perform one gradient step
        if IJprop === nothing
            _, ∇ = GLR(expfam, w, S./N) # evaluate GLR statistic for gradient
            incur!(ah, -∇[:])
        end
    end

    if verbose
        μhat = S ./ N
        println("Stopped after $(sum(N)) samples")
        print("Sample size "); display(N)
        print("Sample prop "); display(N ./ sum(N))
        print("Emp. estimate "); display(μhat)
    end

    res, N, S
end


function tas_proportional(rng, K, J, expfam, df, GLR, β, α, μ, T, δ; verbose=true)

    # mild fake outcomes cheat to regularize Bernoulli
    N = zeros(Int64, K, J) .+ 1
    S = zeros(K, J) .+ 1/2

    # version of exponentiated gradient,
    # used as our online learner for w
    ahs = [AdaHedge(K) for j in 1:J]

    # vector of results
    res = []
    p = 1

    for t in 1:T

        # i.i.d. contexts
        Jt = findfirst(rand(rng) .≤ cumsum(α));
        @assert Jt != nothing

        # forced exploration
        Iprop = findfirst(N[:,Jt] .≤ sqrt(sum(N[:,Jt])))

        It = if Iprop === nothing
            μhat = S./N
            v, _ = GLR(expfam, N, μhat) # evaluate GLR statistic for stopping
            p = minimum((p, pval(v, sum(N), δ)))

            push!(res, (t, v, p, istar(μhat, β) == istar(μ, β))) # if we would stop now ...

            # get the online learner's current weights for context Jt
            w = act(ahs[Jt])

            # direct tracking
            argmin(N[:,Jt] .- (sum(N[:,Jt])+1).*w)
        else
            Iprop
        end

        # take the sample in the Bandit
        action = It - 1
        season = Jt - 1
        S[It, Jt] += df[action][season][N[It, Jt]]
        N[It, Jt] += 1

        # and perform one gradient step
        if Iprop === nothing
            # get the online learner's current weights
            w = hcat(α.*act.(ahs)...) # blow out with factors α
            _, ∇ = GLR(expfam, w, S./N) # evaluate GLR statistic for gradient
            incur!.(ahs, eachcol(- ∇ .* α')) # chain rule
        end
    end

    if verbose
        μhat = S ./ N
        println("Stopped after $(sum(N)) samples")
        print("Sample size "); display(N)
        print("Sample prop "); display(N ./ sum(N))
        print("Emp. estimate "); display(μhat)
    end

    res, N, S
end



function test()
    rng = MersenneTwister(1234)
    expfam = Bernoulli()

    df = myunpickle("df.pkl")  # dictionary df[arm][season]
    K = length(df)
    J = length(df[0])
    α = β = [0.1958,  0.2950,  0.2813,  0.2279]
    T = 14_000_000
    runs=1


    GLR = (expfam, w, μ) -> GLR_allbetter(w, μ, expfam, β, GLR_newton_unstructured)
    # Data statistics, needed to compute characteristic time
    μ = [ 0.0296  0.0372  0.0588  0.062
          0.03    0.0373  0.0596  0.0626
          0.0295  0.0373  0.0591  0.063]
    Dopt, wopt = oracle_active(K, J, Nothing, w -> GLR(expfam, w, μ))
    Dopt_p, wopt_p = oracle_proportional(K, J, α, w -> GLR(expfam, w, μ))
    Dopt_a, wopt_a = oracle_agnostic_orig(K, J, α, w -> GLR(expfam, w, μ))
    GLR_obl = (expfam, w, μ) -> GLR_allbetter(w, μ, expfam, ones(1,1), GLR_newton_unstructured)
    μ_obl = reshape(μ*β, K, 1)
    Dopt_obl, wopt_obl = oracle_agnostic_orig(K, 1, ones(1,1), w -> GLR_obl(expfam, w, μ_obl))

    print("Bandit "); display(μ)
    println("β $β")
    println("Correct answer ", istar(μ, β))
    println("Active")
    println("Characteristic time active: ", 1/Dopt)
    print("Oracle weights active: "); display(round.(wopt, digits=3))
    println("Proportional")
    println("Characteristic time proportional: ", 1/Dopt_p)
    print("Oracle weights proportional: "); display(round.(wopt_p, digits=4))
    println("Agnostic")
    println("Characteristic time agnostic: ", 1/Dopt_a)
    print("Oracle weights agnostic: "); display(wopt_a)
    println("Oblivious")
    println("Characteristic time oblivious: ", 1/Dopt_obl)
    print("Oracle weights oblivious: "); display(wopt_obl)

    δ = 0.1; # only for graph

    println("Active mode")
    @time Q, N, S = tas_active(K, J, expfam, df, GLR, β, Nothing, μ, T, δ; verbose=true)
    plot!(getindex.(Q, 1), getindex.(Q, 3), label="active")
    gui()

    println("Proportional mode")
    @time Q, N, S = tas_proportional(rng, K, J, expfam, df, GLR, β, α, μ, T, δ; verbose=true)
    plot!(getindex.(Q, 1), getindex.(Q, 3), label="proportional")
    gui()

    println("Agnostic mode")
    @time Q, N, S = tas_agnostic(rng, K, J, expfam, df, GLR, β, α, μ, T, δ;)
    plot!(getindex.(Q, 1), getindex.(Q, 3), label="agnostic")
    gui()

    println("Oblivious mode")
    @time Q, N, S = tas_oblivious(rng, K, J, expfam, df, GLR, β, α, μ, T, δ)
    plot!(getindex.(Q, 1), getindex.(Q, 3), label="oblivious")
    gui()

    println("BC-ABC")
    @time Q, N, S = BC_ABC(rng, K, J, expfam, df, GLR, β, α, μ, T, δ)
    plot!(getindex.(Q, 1), getindex.(Q, 3), label="BC-ABC")
    gui()


    println("Uniform")
    @time Q, N, S = uniform(rng, K, J, expfam, df, GLR, β, α, μ, T, δ)
    plot!(getindex.(Q, 1), getindex.(Q, 3), label="uniform")
    gui()


end

test()

