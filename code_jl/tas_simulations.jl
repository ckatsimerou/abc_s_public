using Test, Random, Plots
include("general.jl")
include("libs/tidnabbil/binary_search.jl")
include("libs/tidnabbil/expfam.jl")


using Test, Random, Plots, Distributions
include("general.jl")
include("libs/tidnabbil/regret.jl")



# correct answer function
function istar(μ, β)
    quals = μ*β
    if false # BAI
        argmax(quals)
    else # A/B
        quals[2:end] .≥ quals[1]
    end
end


# compute the GLR with one designated 'base' arm, by
# taking the minimum over all other arms (should be in general.jl)
function GLR_min_arm(w, μ, expfam, base, β, GLR_strucfun)
    minimum((GLR_strucfun(w, μ, expfam, base, oth, β)[1], oth)
            for oth in 1:size(μ,1) if oth ≠ base
            )
end


# inverted stopping threshold
function pval(min_glr, t, δ)
    (log(t) + 1) / exp(min_glr)
end


function generate_bandit(rng, K, J)
	μ = 0.8*rand(rng, Float64, (K, J))  # make the problem a bit harder
	β = ones(J)/J
    α = rand(rng, Dirichlet(J, 10))  #avoid too rare contexts
	i_star = istar(μ, β)
	μ, α, β, i_star
end


function Uniform(rng, expfam, μ, GLR, α, β, T, δ)
  # Chernoff stopping rule, uniform sampling rule
    condition = true
    K, J = size(μ)

    # initialization
    N = zeros(Int64, K, J) .+ 1
    S = zeros(K, J) .+ 1/2
    t_δ = T
    v = 0
    p = 1
    t = 0

    while (condition)
        # i.i.d. contexts
        Jt = findfirst(rand(rng) .≤ cumsum(α));
        @assert Jt != nothing

        # take the sample in the Bandit
        S[:, Jt] .+= sample.(rng, Ref(expfam), μ[:, Jt])
        N[:, Jt] .+= 1

        μhat = S./N
        v, _ = GLR(expfam, N, μhat) # evaluate GLR statistic for stopping
        p = minimum((p, pval(v, sum(N), δ)))

        t = sum(N)

        if t >= T
            t_δ = T;
            condition=false
        end

        if v >= log((log(sum(N))+1)/δ)
            t_δ = t;
            condition = false
        end
    end

    μhat = S./N
    res = (p, minimum((t_δ, T)), v, istar(μhat, β) == istar(μ,β))

    res, N, S
end


function BC_ABC(rng, expfam, μ, GLR, α, β, T, δ)
    # Chernoff stopping rule, Sort of Best Challenger
    condition = true
    K, J = size(μ)

    # initialization
    N = zeros(Int64, K, J) .+ 1
    S = zeros(K, J) .+ 1/2
    t_δ = T
    v = 0
    p = 1
    t = 0

    while (condition)
        # i.i.d. contexts
        Jt = findfirst(rand(rng) .≤ cumsum(α));
        @assert Jt != nothing

       # forced exploration
        Iprop = findfirst(sum(N, dims=2) .≤ sqrt(sum(N)))

        IJ = if Iprop === nothing
            μhat = S./N
            # evaluate GLR statistic for stopping and closest arm for sampling
            v, i = GLR_min_arm(N, μhat, expfam, 1, β, GLR_newton_unstructured)
            p = minimum((p, pval(v, sum(N), δ)))

            if v >= log((log(sum(N))+1)/δ)
                t_δ = t;
                break
            end
            (1, i)
        else
            Iprop[1]
        end

        for ij in 1:length(IJ) # could be just 2
            S[IJ[ij], Jt] += sample(rng, expfam, μ[IJ[ij], Jt])
            N[IJ[ij], Jt] += 1
        end

        t += length(IJ)

    end

    μhat = S./N
    res = (p, minimum((t_δ, T)), v, istar(μhat, β) == istar(μ,β), 0)
    display(N)
    res, N, S
   end


function tas_oblivious(rng, expfam, μ, GLR, α, β, T, δ)
    K, J = size(μ)

    # mild fake outcomes cheat to regularize Bernoulli
    N = zeros(Int64, K, 1) .+ 1
    S = zeros(K, 1) .+ 1/2

    GLR = (expfam, w, μ) -> GLR_allbetter(w, μ, expfam, ones(1,1), GLR_newton_unstructured)

    # version of exponentiated gradient,
    # used as our online learner for w
    ah = AdaHedge(K)

    t_δ = T
    v = 0
    p = 1

    for t in 1:T
        # i.i.d. contexts
        Jt = findfirst(rand(rng) .≤ cumsum(α));
        @assert Jt != nothing

        # forced exploration
        Iprop = findfirst(N .≤ sqrt(sum(N)))

        It = if Iprop === nothing
            μhat = S./N
            v, _ = GLR(expfam, N, μhat) # evaluate GLR statistic for stopping
            p = minimum((p, pval(v, sum(N), δ)))

            if v >= log((log(sum(N))+1)/δ)
                t_δ = t;
                break
            end

            # get the online learner's current weights
            w = reshape(act(ah), K, 1)

            # direct tracking
            argmin(N .- (sum(N)+1).*w)[1]
        else
            Iprop[1]
        end

        # take the sample in the Bandit
        S[It, 1] += sample(rng, expfam, μ[It, Jt])
        N[It, 1] += 1

        # and perform one gradient step
        if Iprop === nothing
            _, ∇ = GLR(expfam, w, S./N) # evaluate GLR statistic for gradient
            incur!(ah, -∇[:])
        end
    end
    μhat = S./N
    res = (p, minimum((t_δ, T)), v, istar(μhat, ones(1,1)) == istar(μ,β))

    res, N, S
end


function tas_agnostic(rng, expfam, μ, GLR, α, β, T, δ)
    K, J = size(μ)

    # mild fake outcomes cheat to regularize Bernoulli
    N = zeros(Int64, K, J) .+ 1
    S = zeros(K, J) .+ 1/2

    # version of exponentiated gradient,
    # used as our online learner for w
    ah = AdaHedge(K)

    t_δ = T
    v = 0
    p = 1

    for t in 1:T
        # i.i.d. contexts
        Jt = findfirst(rand(rng) .≤ cumsum(α));
        @assert Jt != nothing

        # forced exploration
        Iprop = findfirst(sum(N, dims=2) .≤ sqrt(sum(N)))

        It = if Iprop === nothing
            μhat = S./N
            v, _ = GLR(expfam, N, μhat) # evaluate GLR statistic for stopping
            p = minimum((p, pval(v, sum(N), δ)))

            if v >= log((log(sum(N))+1)/δ)
                t_δ = t;
                break
            end

            # get the online learner's current weights
            w = act(ah)

            # direct tracking
            argmin(sum(N, dims=2) .- (sum(N)+1).*w)[1]
        else
            Iprop[1]
        end

        # take the sample in the Bandit
        S[It, Jt] += sample(rng, expfam, μ[It, Jt])
        N[It, Jt] += 1

        # and perform one gradient step
        if Iprop === nothing
            _, ∇ = GLR(expfam, w*α', S./N) # evaluate GLR statistic for gradient
            incur!(ah, -∇*α)
        end
    end
    μhat = S./N
    res = (p, minimum((t_δ, T)), v, istar(μhat, β) == istar(μ,β))

    res, N, S
end


function tas_active(rng, expfam, μ, GLR, α, β, T, δ)
    K, J = size(μ)

    # mild fake outcomes cheat to regularize Bernoulli
    N = zeros(Int64, K, J) .+ 1
    S = zeros(K, J) .+ 1/2

    # version of exponentiated gradient,
    # used as our online learner for w
    ah = AdaHedge(K*J)

    # vector of results
    t_δ = T
    v = 0
    p = 1

    for t in 1:T

        # forced exploration
        IJprop = findfirst(N .≤ sqrt(sum(N)))

        IJ = if IJprop === nothing

	        μhat = S./N
	        v, _ = GLR(expfam, N, μhat) # evaluate GLR statistic for stopping
	        p = minimum((p, pval(v, sum(N), δ)))

            if v >= log((log(sum(N))+1)/δ)
                t_δ = t;
                break
            end

            # get the online learner's current weights
            w = reshape(act(ah), K, J)

            # direct tracking
            argmin(N .- (sum(N)+1).*w)
        else
            IJprop
        end

        # take the sample in the Bandit
        S[IJ] += sample(rng, expfam, μ[IJ])
        N[IJ] += 1

        # and perform one gradient step
        if IJprop === nothing
            _, ∇ = GLR(expfam, w, S./N) # evaluate GLR statistic for gradient
            incur!(ah, -∇[:])
        end
    end
    μhat = S ./ N
    res = (p, minimum((t_δ, T)), v, istar(μhat, β) == istar(μ,β))

    res, N, S
end

function tas_proportional(rng, expfam, μ, GLR, α, β, T, δ)
    K, J = size(μ)

    # mild fake outcomes cheat to regularize Bernoulli
    N = zeros(Int64, K, J) .+ 1
    S = zeros(K, J) .+ 1/2

    # version of exponentiated gradient,
    # used as our online learner for w
    ahs = [AdaHedge(K) for j in 1:J]

    t_δ = T
    v = 0
    p = 1

    # vector of results

    for t in 1:T

        # i.i.d. contexts
        Jt = findfirst(rand(rng) .≤ cumsum(α));
        @assert Jt != nothing

        # forced exploration
        Iprop = findfirst(N[:,Jt] .≤ sqrt(sum(N[:,Jt])))

        It = if Iprop === nothing
            μhat = S./N
            v, _ = GLR(expfam, N, μhat) # evaluate GLR statistic for stopping
            p = minimum((p, pval(v, sum(N), δ)))

            if v >= log((log(sum(N))+1)/δ)
                t_δ = t;
                break
            end

            # get the online learner's current weights for context Jt
            w = act(ahs[Jt])

            # direct tracking
            argmin(N[:,Jt] .- (sum(N[:,Jt])+1).*w)
        else
            Iprop
        end

        # take the sample in the Bandit
        S[It, Jt] += sample(rng, expfam, μ[It, Jt])
        N[It, Jt] += 1

        # and perform one gradient step
        if Iprop === nothing
            # get the online learner's current weights
            w = hcat(α.*act.(ahs)...) # blow out with factors α
            _, ∇ = GLR(expfam, w, S./N) # evaluate GLR statistic for gradient
            incur!.(ahs, eachcol(- ∇ .* α')) # chain rule
        end
    end
    μhat = S./N
    res = (p, minimum((t_δ, T)), v, istar(μhat, β) == istar(μ,β))

    res, N, S
end


function run_simulation_methods()
    rng = MersenneTwister(1234)
    expfam = Bernoulli()
    K = 4
    J = rand(rng, 3:10)
    δ = 0.1
    T = 100000  #truncation time
    runs = 3000
    methods = [tas_active, tas_proportional, tas_agnostic, BC_ABC, Uniform]
    results = zeros(runs, length(methods), 5)
    for rep in 1:runs
        display(rep)
    	μ, α, β, i_star = generate_bandit(rng, K, J)
    	@assert sum(α) ≈ 1
    	GLR = (expfam, w, μ) -> GLR_allbetter(w, μ, expfam, β, GLR_newton_unstructured)
        for imeth in 1:length(methods)
            method = methods[imeth]
    		Q, N, S = method(rng, expfam, μ, GLR, α, β, T, δ)
            results[rep, imeth, :] .= Q
        end
    end
    results
end

function run_one_model()
    rng = MersenneTwister(1234)
    expfam = Bernoulli()
    δ = 0.1
    K = 3
    J = 3
    GLR = (expfam, w, μ) -> GLR_allbetter(w, μ, expfam, β, GLR_newton_unstructured)
    μ = [0.1 0.4 0.3
         0.2 0.5 0.2
         0.5 0.1 0.1]
    β  = ones(J)/J
    α = [0.4, 0.5, 0.1]
    Dopt, wopt = oracle_active(K, J, Nothing, w -> GLR(expfam, w, μ))
    Dopt_p, wopt_p = oracle_proportional(K, J, α, w -> GLR(expfam, w, μ))
    Dopt_a, wopt_a = oracle_agnostic_orig(K, J, α, w -> GLR(expfam, w, μ))

    print("Bandit "); display(μ)
    println("β $β")
    println("Correct answer ", istar(μ, β))
    println("Active")
    println("Characteristic time active: ", 1/Dopt)
    print("Oracle weights active: "); display(wopt)
    println("Proportional")
    println("Characteristic time proportional: ", 1/Dopt_r)
    print("Oracle weights proportional: "); display(wopt_r)
    println("Agnostic")
    println("Characteristic time agnostic: ", 1/Dopt_a)
    print("Oracle weights agnostic: "); display(wopt_a)


    T = 100000  #truncation time
    runs = 1000
    methods = [tas_active, tas_proportional, tas_agnostic, BC_ABC, ChernoffUniform]
    results = zeros(runs, length(methods), 5)
    for rep in 1:runs
        display(rep)
        for imeth in 1:length(methods)
            method = methods[imeth]
            display(method)
            Q, N, S = method(rng, expfam, μ, GLR, α, β, T, δ)
            results[rep, imeth, :] .= Q
        end
    end
    results
end
