# Aim:
# Implement track-and-stop with
# - seasonality
# - best arm and all-better-than-baseline
# - structure
# - general exp. fam. distributions

# Good news:
#
# 1) we only have to implement a GLR oracle, as we can compute the
# optimal allocation w^* on top of that (using e.g. ellipsoid)
#
# 2) best-arm and all-arms-better-than-the-baseline are related
# correct-answer functions. Namely, to get to an alternative model
# where the best arm is different, we need to flip the current best
# arm and some other arm. To get to an alternative model where the set
# of arms-better-than-the-baseline is different, we need to swap the
# baseline and some other arm.
#
# So it suffices to be able to compute the closest alternative with
# two designated arms swapped.
#
# 3) Having no seasonality is the special case of one season.
#
#
#
# Notation (μ)
# A bandit model μ is matrix of K × J means, with K actions and J days.
#
# Notation (α and β):
# We say that action a is better than b if μ[a,:]⋅β ≥ μ[b]⋅β.
# We also sometimes impose the constraint that \sum_i w_{i,j} = α_j
# There is no reason for α and β to be related.


using Plots, LinearAlgebra, SparseArrays, Test, Random
include("libs/tidnabbil/expfam.jl")
include("libs/ellipse/ellipse_core.jl")
include("quad_opt.jl");



############################ AUX ############################
#
# we start with some auxiliary functionality, to support optimising
# over the simplex using the ellipsoid method

# positive orthant constraint, represented by
# constraint violation and separating hyperplane
function positive(w)
    K = length(w);
    maximum((-w[i], -I(K)[:,i]) for i in 1:K)
end

# ellipse (center and matrix) enclosing the K-simplex
function simplex_ball(K)
    u = ones(K)./K;
    P = (1-1/K)*I(K);
    for i in 1:K
        ei = I(K)[:,i];
        @assert (ei-u)'inv(P)*(ei-u) ≈ 1
    end
    u, P
end


# numerically verify gradient of differentiable function
function test_exact_grad(K, f)
    for rep = 1:100
        w = diff([0, sort(rand(K-1))..., 1]);
        eps = 1e-7;

        v, ∇ = f(w);
        @test [(f(collect(w .+ eps*I(K)[:,i]))[1] - v)/eps for i=1:K] ≈ ∇ atol=1e-3 rtol=1e-3
    end
end

# numerically verify supergradient of concave function
function test_supgrad(K, f)
    minus(x) = -x
    test_subgrad(K, w -> minus.(f(w)))
end

# numerically verify subgradient of convex function
function test_subgrad(K, f)
    xf∇ = [ begin
            w = diff([0, sort(rand(K-1))..., 1])
            (w, f(w)...)
            end
            for rep in 1:100];

    for (w1, f1, ∇1) in xf∇
        for (w2, f2, ∇2) in xf∇
            @test ∇1⋅(w2-w1) ≤ f2-f1 + 1e-7 # subgradient of convex function
        end
    end
end


# numerically verify that a given point w maximises f
function test_max(f, w)
    K = length(w);
    v,_ = f(w); # claimed maximum
    for rep = 1:100
        w2 = diff([0, sort(rand(K-1))..., 1]);
        β = rand();
        v2, _ = f((1-β).*w .+ β.*w2);
        @test v ≥ v2 - 1e-3
    end
end


# maximise the concave function f over the K-simplex
# f must return (value, gradient).
function max_simplex(K, f)
    u, P = simplex_ball(K)

    fA, consA, cA, PA, w, x = unitsum_wrap(f, positive, u, P)
    optX, (optv, optg) = ellipse_maximise(cA, PA, fA, consA; tol=1e-7, verbose=false, maxiter=50000)

    optv, w(optX)
end




############################ STRUCTURE ############################
#
# We currently have one structure: 'unstructured'
# in unstructured, λ_{i,j} are arbitrary



# Unstructured oracle
# A bandit model is an actions × days table of means.
# For bandit λ, action a being swapped with b means λ[a,:]⋅β = λ[b,:]⋅β
#
# Minimize
#    \sum_{i,j} w_{i,j} d(μ_{i,j}, λ_{i,j})
# over bandit models λ subject to
#    λ[a,:]⋅β = λ[b,:]⋅β
# note that we can ignore actions outside {a,b}.

function GLR_unstructured_slow(w, μ, expfam, a, b, β)
    K, J = size(μ)

    f(Λ) = let λ = reshape(Λ, K, J)
        w ⋅ d.(Ref(expfam), μ, λ), (w .* dλ_d.(Ref(expfam), μ, λ))[:]
    end

    # big ball
    u, P = μ[:], 10*I(K*J);

    # range constraints from expfam
    cons(Λ) = begin
        if expfam isa Poisson || expfam isa Bernoulli
            v, ix = findmin(Λ);
            if v < 0
                return -v, -I(K*J)[:,ix]
            end
        end

        if expfam isa Bernoulli
            v, ix = findmax(Λ);
            if v > 1
                return v-1, I(K*J)[:,ix]
            end
        end
        0, spzeros(K*J)
    end

    # linear constraint: (λ[a,:] .- λ[b,:])⋅β = 0
    A = collect(Float64, kron(β, (1:K.==a) .- (1:K.==b))');
    fA, consA, cA, PA, q, x = equality_wrap(f, cons, u, P, A, zeros(1,1));
    optX, (optv, optg) = ellipse_minimise(cA, PA, fA, consA; tol=1e-7, verbose=false, maxiter=10000)

    # this returns gradient in w
    let λ = reshape(q(optX), K, J)
        optv, d.(Ref(expfam), μ, λ)
    end
end


# strip down to the only 2 arms relevant in the unstructured case
function GLR_unstructured(w, μ, expfam, a, b, β)
    K, J = size(w)
    v, ∇ab = GLR_unstructured_slow(w[[a,b],:], μ[[a,b],:], expfam, 1, 2, β)
    v, I(K)[a,:] * ∇ab[1,:]' .+ I(K)[b,:] * ∇ab[2,:]'
end




function GLR_newton_unstructured_slow(w, μ, expfam, a, b, β; verbose = false)
    K, J = size(w)

    # feasible starting pt
    λ = copy(μ);
    λ[[a,b],:] .= (w[[a,b],:]⋅μ[[a,b],:])/sum(w[[a,b],:]);

    B = collect(Float64, kron(β', (1:K.==a) .- (1:K.==b)));

    ∇, H, s = zeros(K, J), zeros(K, J), zeros(K, J); # preallocate

    for it in Iterators.countfrom()
        # iterate is feasible
        @assert abs(B⋅λ) < 1e-7 "got $(abs(B⋅λ))"

        v = w ⋅ d.(Ref(expfam), μ, λ);
        ∇ .= w .* dλ_d.(Ref(expfam), μ, λ);
        H .= w .* dλ2_d.(Ref(expfam), μ, λ);

        # Newton step (with our single equality constriant)
        s .= (∇ .- ((B⋅(∇./H))/(B⋅(B./H))) .* B)./H;
        Nd² = s⋅(H.*s); # sq. Newton decrement

        # local quadratic appx. has minimum value v-Nd²/2
        # and this is also what we hope to get in a Newton iteration

        # backtracking line search
        α = 1.
        while true
            if α*(1-α/2)*Nd²/2 ≤ v*1e-10 # progress not worth it
                return w ⋅ d.(Ref(expfam), μ, λ), d.(Ref(expfam), μ, λ)
            end
            if v - w⋅d.(Ref(expfam), μ, λ .- α.*s) ≥ α*(1-α/2)*Nd²/2
                break # progress achieved
            end
            α /= 2 # half the target
        end

        if verbose
            println("iteration $it  value $v  Nd² $Nd²  α $α  bd $(α*(1-α/2)*Nd²/2)")
        end

        if it == 40
            @warn "Reached $it iterations. Trouble?"
            # if !verbose
            #     println("---VERBOSE REPEAT---")
            #     GLR_newton_unstructured_slow(w, μ, expfam, a, b, β; verbose=true)
            # else
            #     @assert false
            # end
        end

        λ .-= α .* s;
    end
end

# strip down to the only 2 arms relevant in the unstructured case
function GLR_newton_unstructured(w, μ, expfam, a, b, β; kwargs...)
    K, J = size(w)
    v, ∇ab = GLR_newton_unstructured_slow(w[[a,b],:], μ[[a,b],:], expfam, 1, 2, β; kwargs...)
    g = spzeros(K, J);
    g[[a,b],:] .= ∇ab
    v, g
end



if false
    # Experiment with Convex.jl. It is too slow.
    using Convex, SCS, MathOptInterface

    function GLR_Convex_unstructured_slow(w, μ, expfam, a, b, β; verbose = false)
        λ = Variable(size(μ))
        problem = minimize(w ⋅ square(μ - λ)/2, [(λ[a,:]-λ[b,:])*β == 0])
        solve!(problem, SCS.Optimizer, silent_solver=true)
        @assert problem.status ∈ (MathOptInterface.OPTIMAL, MathOptInterface.ALMOST_OPTIMAL, MathOptInterface.ALMOST_DUAL_INFEASIBLE) "got $(problem.status) of type $(typeof(problem.status))"

        problem.optval, d.(Ref(expfam), μ, λ.value)
    end

    # strip down to the only 2 arms relevant in the unstructured case
    function GLR_Convex_unstructured(w, μ, expfam, a, b, β)
        K, J = size(w)
        v, ∇ab = GLR_Convex_unstructured_slow(w[[a,b],:], μ[[a,b],:], expfam, 1, 2, β)
        v, I(K)[a,:] * ∇ab[1,:]' .+ I(K)[b,:] * ∇ab[2,:]'
    end
end



############################ ANSWER ############################
#
# We currently have two correct answer functions: 'bestact' and 'allbetter'
# in bestact, the answer is the index of the best action
# in allbetter, the answer is the set of actions better than the baseline
#
# note that in either case, action a is better than b if μ[a,:]⋅β ≥ μ[b,:]⋅β



# compute the GLR with one designated 'base' arm, by
# taking the minimum over all other arms
function GLR_min(w, μ, expfam, base, β, GLR_strucfun)
    argmin(first,
           GLR_strucfun(w, μ, expfam, base, oth, β)
           for oth in 1:size(μ,1) if oth ≠ base
           )
end


function GLR_bestact(w, μ, expfam, β, GLR_strucfun)
    # best arm takes role of baseline
    GLR_min(w, μ, expfam, argmax(μ*β), β, GLR_strucfun)
end

function GLR_allbetter(w, μ, expfam, β, GLR_strucfun; baseline=1)
    # the first action is the baseline action
    GLR_min(w, μ, expfam, baseline, β, GLR_strucfun)
end



############################ MODE ############################
#
# Modes are constraints on the sampling weights.
# We currently have three modes: 'active', 'proportional' and 'agnostic'.
# active puts no constraints on w_{i,j} beyond being on the simplex
# proportional asks that \sum_i w_{i,j} = α_j for some distribution α on days
# agnostic asks that w_{i,j} = w_i α_j for some distribution α on days



# in active mode w_{i,j} is arbitrary. Note that α is ignored.
function oracle_active(K, J, α, GLRfn)
    f = W -> let w = reshape(W, K, J)
        v, ∇ = GLRfn(w)
        v, ∇[:]
    end

    optv, optw = max_simplex(K*J, f);

    optv, reshape(optw, K, J)
end


# in proportional mode w_{i,j} is constrained so that sum_i w_{i,j} = α_j
function oracle_proportional(K, J, α, GLRfn)
    f = W -> let w = reshape(W, K, J)
        v, ∇ = GLRfn(w)
        v, ∇[:]
    end

    u, P = simplex_ball(K*J)

    # for all j: sum_i w_{i,j} = α_j
    A = kron(I(J), ones(1,K))
    fA, consA, cA, PA, w, x = equality_wrap(f, positive, u, P, A, α);
    optX, (optv, optg) = ellipse_maximise(cA, PA, fA, consA; tol=1e-7, verbose=false, maxiter=10000)

    optv, reshape(w(optX), K, J)
end


# in agnostic mode w_{i,j} is w_i * α_j
# that is, the action is independent of the day
function oracle_agnostic_orig(K, J, α, GLRfn)
    f = w -> begin
        v, ∇ = GLRfn(w*α')
        v, ∇*α
    end

    optv, optw = max_simplex(K, f)
end

# same as above, but with weights blow out (to compare to active/proportional)
function oracle_agnostic(K, J, α, GLRfn)
    v, w = oracle_agnostic_orig(K, J, α, GLRfn)
    v, w*α'
end



############################ TRY IT ############################
#
# here we test the above code.

if false
@testset "order $expfam - $answer - $struc" for
        expfam in (Gaussian(), Bernoulli(), Poisson()),
        answer in (GLR_bestact, GLR_allbetter),
        struc in (GLR_unstructured)
    K = 3; # actions
    J = 3; # days

    μ = rand(K, J);
    α = diff([0, sort(rand(J-1))..., 1]);
    β = randn(J);

    GLRfn = w -> answer(w, μ, expfam, β, struc);
    vac, vp, vag = (mode(K, J, α, GLRfn)[1]
                  for mode in (oracle_active, oracle_proportional, oracle_agnostic));

    # active ≥ proportional ≥ agnostic
    @test vac ≥ vp - 1e-5
    @test vp ≥ vag - 1e-5
end


@testset "gradients $struc" for struc in (GLR_unstructured)
    K = 5; # actions
    J = 5; # days

    for rep in 1:10
        μ = rand(K, J);
        expfam = Bernoulli();
        β = randn(J);

        test_supgrad(K*J, W -> let w = reshape(W, K, J)
                     v, ∇ = struc(w, μ, expfam, 1, 2, β)
                     v, ∇[:]
                     end
                     )
    end
end
end

# the below functions are WIP

function speedtest()
    K = 5; # actions
    J = 5; # days

    μ = rand(K, J);
    expfam = Bernoulli();
    w = rand(K, J);
    β = ones(J);

    @time GLR_unstructured(w, μ, expfam, 1, 2, β)
end



function isuniform()
    K = 4; # actions
    J = 3; # days

    μ = rand(K, J);
    expfam = Gaussian();
    α = ones(J)/J;
    β = α;

    GLRfn = w -> GLR_allbetter(w, μ, expfam, β, GLR_newton_unstructured);
    for oracle in (oracle_active, oracle_proportional, oracle_agnostic)
        v, w = oracle(K, J, α, GLRfn)
        println("$oracle  $v");
        display(w)
    end
end


if false
@testset "$ellipsoid vs $newton " for (ellipsoid, newton) in ((GLR_unstructured,GLR_newton_unstructured))
    for it in 1:1000
        K = 3; # actions
        J = 5; # days

        μ = rand(K, J);
        expfam = Bernoulli();
        w = rand(K, J);
        β = randn(J);

        ve, ∇e = ellipsoid(w, μ, expfam, 1, 2, β)
        vn, ∇n =    newton(w, μ, expfam, 1, 2, β)

        if !(vn ≤ ve*(1+1e-5))
            println("it is $it");
            display(ve)
            display(collect(∇e))
            display(vn)
            display(∇n)
        end

        # test that Newton beats ellipsoid
        @test vn ≤ ve*(1+1e-5)
    end
end
end
